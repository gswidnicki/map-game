var obiekt = {};

init = function (){
	'use strict';
	var formularz = document.getElementById('form-create-map');
	formularz.addEventListener('submit', function (e) {
		e.preventDefault();
		var sizeX = document.getElementById('sizeX').value;
		var sizeY = document.getElementById('sizeY').value;
		var createPlayfield = function() {
			var container = document.getElementById("playField");
			var tabela = document.createElement("table");
			tabela.setAttribute("border", 1);
			var tbody = document.createElement("tbody");
			for (var i = 1; i <= sizeY; ++i) {
				var tr = document.createElement("tr");
				for (var j = 1; j <= sizeX; ++j) {
					var td = document.createElement("td");
					td.setAttribute("border", 1);
					td.setAttribute("id", "x"+j+"y"+i);
					tr.appendChild(td);
				}
				tbody.appendChild(tr);
			}
			tabela.appendChild(tbody);
			container.appendChild(tabela);
		}
		var createMoveButton = function(text) {
			return $('<button>', { html: text });
		}
		if (!isNaN(sizeX) && !isNaN(sizeY)) {
			createPlayfield();
		}else{
			alert("Podane wartości są błędne!");
		}
		obiekt.x = Math.ceil(Math.random() * sizeX);
		obiekt.y = Math.ceil(Math.random() * sizeY);
		$('#'+idForObject(obiekt)).css('background-color', "red");
		$('#movingObiekt').append(["LEFT", "RIGHT", "UP", "DOWN"].map(v => createMoveButton(v)));
	});
}

idForObject = function(obj){
	return "x"+obj.x+"y"+obj.y;
}

turnLeft = function (number) {
	$('#'+idForObject(obiekt)).css('background-color', 'white');
	obiekt.x -= number;
	$('#'+idForObject(obiekt)).css('background-color', "red");
}
turnRight = function (number) {
	$('#'+idForObject(obiekt)).css('background-color', 'white');
	obiekt.x += number;
	$('#'+idForObject(obiekt)).css('background-color', "red");
}
turnUp = function (number) {
	$('#'+idForObject(obiekt)).css('background-color', 'white');
	obiekt.y -= number;
	$('#'+idForObject(obiekt)).css('background-color', "red");
}
turnDown = function (number) {
	$('#'+idForObject(obiekt)).css('background-color', 'white');
	obiekt.y += number;
	$('#'+idForObject(obiekt)).css('background-color', "red");
}
